CREATE TABLE blog_posts (
  ID   INTEGER PRIMARY KEY,
  title text,
  description text,
  content text,
  is_public int
);
