package manageCommands

import (
	"context"
	"database/sql"
	"fmt"
	"os"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/rdubwiley/ssh-blog/models"
)

var SqlDbLoc = "db.db"

type CreateBlogPostConfig struct {
	title       string
	description string
	contentLoc  string
	isPublic    int
}

func NewCreateBlogPostConfig(args []string) CreateBlogPostConfig {
	cbpc := CreateBlogPostConfig{
		title:       args[0],
		description: args[1],
		contentLoc:  args[2],
	}
	if len(args) == 4 {
		isPublic, err := strconv.Atoi(args[3])
		if err != nil {
			panic(err)
		}
		cbpc.isPublic = isPublic
	} else {
		cbpc.isPublic = 1
	}
	return cbpc
}

func CreateBlogPost(config CreateBlogPostConfig) {
	b, err := os.ReadFile(config.contentLoc)
	content := string(b)
	if len(content) == 0 {
		fmt.Println("You must supply a valid markdown file")
		return
	}
	db, err := sql.Open("sqlite3", SqlDbLoc)
	if err != nil {
		fmt.Println("Couldn't upload file")
		return
	}
	ctx := context.Background()
	queries := models.New(db)
	insertedBlogPost, err := queries.CreateBlogPost(ctx, models.CreateBlogPostParams{
		Title:       sql.NullString{config.title, true},
		Description: sql.NullString{config.description, true},
		Content:     sql.NullString{content, true},
		IsPublic:    sql.NullInt64{int64(config.isPublic), true},
	})
	if err != nil {

		fmt.Println("Could not insert content into database")
	} else {
		out := fmt.Sprintf("Created blog with id %d\n", insertedBlogPost.ID)
		fmt.Println(out)
	}
}

func runCreateBlogPost(cmd *cobra.Command, args []string) {
	CreateBlogPost(NewCreateBlogPostConfig(args))
}

//TODO: implement the rest of these DB operations
/*

func ActivateBlogPost(id int) {

}

func InactivateBlogPost(id int) {

}

func ListBlogPosts() {

}
*/
