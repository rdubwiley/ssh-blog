package manageCommands

import "github.com/spf13/cobra"

var (
	RunCreateBlogPostCommand = &cobra.Command{
		Use:   "create-blogpost [title] [description] [contentLoc] [isPublic]",
		Short: "Uploads the blog post to the db with the given data",
		Long:  `Uploads the blog post to the db with the given data`,
		Args:  cobra.MinimumNArgs(3),
		Run:   runCreateBlogPost,
	}
)
