# SSH-blog: Template to Start Your Own Blog Over SSH

![](./screenshots/blog.gif)

## Overview
CharmCLI has built a nice [list](https://github.com/charmbracelet/bubbles/tree/master/list) component and they've built [glamour](https://github.com/charmbracelet/glamour).

We put them together with [wish](https://github.com/charmbracelet/wish) to create a blog over SSH.

## Setup
You will need to create a local sqlite db
```bash
sqlite3 db.db
```
And then you will need to create the schema
```sql
CREATE TABLE blog_posts (
  ID   INTEGER PRIMARY KEY,
  title text,
  description text,
  content text,
  is_public int
);
```
From there write your markdown and upload it using the manager
```bash
go run manage create-blogpost "title" "description" "markdown loc"
```

## Further reading
Take a look at [this](https://gitlab.com/rdubwiley/ssh-blog/-/blob/master/example_posts/how_to_start_your_own_ssh_blog.md?ref_type=heads) to get an idea of how to get started locally.

And then look at [this](https://gitlab.com/rdubwiley/ssh-blog/-/blob/master/example_posts/deploying_your_own_ssh_blog.md?ref_type=heads) to deploy the blog to something like Digital Ocean.

## Possible Development/Next Steps
1. Fleshing out the manage CLI to make uploading the blog posts easier
2. Better styling of the list and blog
3. Analytics on blog views and blog post views