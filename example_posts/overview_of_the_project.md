# Introduction To The Blog

This blog demonstrates the capabilitiy of serving a blog over ssh using wish along with a component to serve blogs.

Check out the repo [here](https://gitlab.com/rdubwiley/ssh-blog)

For the sshblogger component check out the repo [here](https://gitlab.com/rdubwiley/sshblogger)