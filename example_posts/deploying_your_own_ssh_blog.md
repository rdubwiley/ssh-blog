# Deploying Your SSH Blog

## Introduction
In this post we're going to show you how to spin up an ubuntu server on something like Digital Ocean and deploy the blog over ssh.

## Prerequisites
1. A server with ssh enabled
2. Ability to sudo to change the ssh ports
3. Already installed go on your server (see [here](https://go.dev/doc/install))
4. Optional but not required is a domain and DNS setup to give your server a nice name

## 1. Changing the SSH port
Because we want users to specify a domain or server ip without needing to know a port number,
we need to change ssh off default port 22.

#### We need to navigate to sshd_config and change our default port.
```bash
sudo su #If not root already
cd /etc/ssh
vim sshd_config
```
#### In here we need to change the line
```bash
#Port 22
```
#### Into something like 
```bash
Port 2234
```
#### and then restart the ssh service with
```
ssh service restart
```

## 2. Cloning the repo and building it
#### If you haven't already, clone the repo
```bash
git clone https://gitlab.com/rdubwiley/ssh-blog
```

#### Let's make sure we have all the needed dependencies with 
```
go mod tidy
```

#### We then need to change the port in the main file AKA this:
```go
const (
	host     = "0.0.0.0"
	port     = "23234"
	SqlDbLoc = "./db.db"
)
```
#### To this:
```go
const (
	host     = "0.0.0.0"
	port     = "22"
	SqlDbLoc = "./db.db"
)
```
#### From there let's build our binary
```bash
go build
```

#### Note that because we need to install sqlite you may need to run:
```bash
sudo apt get gcc
export CGO_ENABLED=1
```

### I recommend doing editing of the markdown on a local machine and copying the sqlite db over
```bash
scp -p 2234 ./dbdb <user>@<ip>:<ssh_dir>
```

#### Finally, I would recommend trying to run the binary (you may need to sudo to get it on port 22)
```bash
sudo ./ssh-blog
```
See if you can ssh and see your content.

## 3. Setting up systemd
#### We need to copy:
```bash
[Unit]
Description=texty App
Requites=network-online.target
After=network-online.target

[Service]
User=<user>
Group=<group>
Type=simple
Restart=always
RestartSec=1
WorkingDirectory=<your working directory>
ExecStart=<where the binary goes>
Restart=on-failure

[Install]
WantedBy=multi-user.target
```
Into /etc/systemd/system/ssh-blog.service. Then fill in the values with the relevant paths and field.

#### From there we just need to set up our service:
```
sudo systemctl daemon-reload
sudo systemctl start ssh-blog
```

If everything is set up correctly you should be able to ssh into your blog!

## Conclusion

As you can see it's not too bad to change SSH ports and deploy our service.

If you have any questions or comments feel free to leave an issue at https://gitlab.com/rdubwiley/ssh-blog

