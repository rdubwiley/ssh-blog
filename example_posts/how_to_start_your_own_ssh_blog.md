# Starting Your Own SSH Blog

## Prerequisites
1. Have some server where you are exposing your blog to the greater internet
2. Have go installed on your server
3. Have a server with SSH set up (probably openssh)

## 1. Clone the server from my gitlab
```bash
git clone https://gitlab.com/rdubwiley/ssh-blog
```

This will give you everything to serve your blog over SSH.

## 2. Initialize the db
Go into the folder you just cloned
```bash
cd ssh-blog
```
#### Now we need to create a new sqlite db called db.db and create our blog post table
```bash
sqlite3 db.db
```
```sql
CREATE TABLE blog_posts (
    ID   INTEGER PRIMARY KEY,
    title text,
    description text,
    content text,
    is_public int
);
```

## 3. Create some content
I recommend creating a folder named posts and writing the content there.
Note that the content should be a markdown file (though I guess you could upload plaintext but it won't be rendered)

## 4. Upload to the db using the cli
#### After you've written your blog post upload it to the sqlite db with:
```bash
go run ./manage create-blogpost "title" "description" "post location"
```
Note that by default your blog post is set as public (public posts are visible on your blog).
You can optionally upload more markdown files to your blog.
The getBlogPosts function gets a reversed list by ID so the last uploaded will be your first in the list.

## 5. Upload a ed25519 public/private keypair to the .ssh folder
#### You can either copy a pair you've already made or create a new one with:
```bash
ssh-keygen -t ed25519
```
We need these to give a public identity for our SSH server

## 6. Try to run locally to verify the server works
```bash
go run .
```
#### You should now be able to ssh locally
```bash
ssh localhost -p 23234
```
Verify your blog post is uploaded and congrats you have a server running that serves markdown blogposts over ssh.
In the next post I talk about how to set up systemd to deploy our server as a service.