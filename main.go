package main

//This serves the texty app
//I left most of the comments from the bubbletea example for clarity

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/log"
	"github.com/charmbracelet/ssh"
	"github.com/charmbracelet/wish"
	"github.com/charmbracelet/wish/activeterm"
	"github.com/charmbracelet/wish/bubbletea"
	"github.com/charmbracelet/wish/logging"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/rdubwiley/ssh-blog/models"
	"gitlab.com/rdubwiley/sshblogger"
)

const (
	host     = "0.0.0.0"
	port     = "23234"
	SqlDbLoc = "./db.db"
)

func getBlogposts() []sshblogger.BlogItem {
	db, err := sql.Open("sqlite3", SqlDbLoc)
	if err != nil {
		fmt.Println(err)
		return []sshblogger.BlogItem{}
	}
	ctx := context.Background()
	queries := models.New(db)
	blogPosts, err := queries.ListPublicBlogPosts(ctx)
	blogItems := []sshblogger.BlogItem{}
	for _, item := range blogPosts {
		blogItems = append(blogItems, sshblogger.BlogItem{
			Id:          int(item.ID),
			Title:       item.Title.String,
			Description: item.Description.String,
			Content:     item.Content.String,
		})
	}
	return blogItems
}

func main() {
	s, err := wish.NewServer(
		wish.WithAddress(net.JoinHostPort(host, port)),
		wish.WithHostKeyPath(".ssh/id_ed25519"),
		wish.WithPublicKeyAuth(func(ctx ssh.Context, key ssh.PublicKey) bool {
			return key.Type() == "ssh-ed25519"
		}),
		wish.WithMiddleware(
			bubbletea.Middleware(teaHandler),
			activeterm.Middleware(), // Bubble Tea apps usually require a PTY.
			logging.Middleware(),
		),
	)
	if err != nil {
		log.Error("Could not start server", "error", err)
	}

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	log.Info("Starting SSH server", "host", host, "port", port)
	go func() {
		if err = s.ListenAndServe(); err != nil && !errors.Is(err, ssh.ErrServerClosed) {
			log.Error("Could not start server", "error", err)
			done <- nil
		}
	}()

	<-done
	log.Info("Stopping SSH server")
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer func() { cancel() }()
	if err := s.Shutdown(ctx); err != nil && !errors.Is(err, ssh.ErrServerClosed) {
		log.Error("Could not stop server", "error", err)
	}
}

// You can wire any Bubble Tea model up to the middleware with a function that
// handles the incoming ssh.Session. Here we just grab the terminal info and
// pass it to the new model. You can also return tea.ProgramOptions (such as
// tea.WithAltScreen) on a session by session basis.
func teaHandler(s ssh.Session) (tea.Model, []tea.ProgramOption) {
	// This should never fail, as we are using the activeterm middleware.
	pty, _, _ := s.Pty()

	// When running a Bubble Tea app over SSH, you shouldn't use the default
	// lipgloss.NewStyle function.
	// That function will use the color profile from the os.Stdin, which is the
	// server, not the client.
	// We provide a MakeRenderer function in the bubbletea middleware package,
	// so you can easily get the correct renderer for the current session, and
	// use it to create the styles.
	// The recommended way to use these styles is to then pass them down to
	// your Bubble Tea model.
	renderer := bubbletea.MakeRenderer(s)
	style := renderer.NewStyle()
	m := sshblogger.NewBlogModel(getBlogposts, "Ryan's blog", pty.Window.Width, pty.Window.Height, pty.Window.Height, style)

	return m, []tea.ProgramOption{tea.WithAltScreen()}
}
