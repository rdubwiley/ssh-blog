-- name: GetBlogPostById :one
SELECT * FROM blog_posts
WHERE ID = ? LIMIT 1;

-- name: ListBlogPosts :many
SELECT * FROM blog_posts
ORDER BY ID DESC;

-- name: ListPublicBlogPosts :many
SELECT * FROM blog_posts
WHERE is_public=1
ORDER BY ID DESC;

-- name: CreateBlogPost :one
INSERT INTO blog_posts (
  title, description, content, is_public
) VALUES (
  ?, ?, ?, ?
)
RETURNING *;

-- name: InactivateBlogPost :exec
UPDATE blog_posts
set is_public = 0
WHERE ID = ?;

-- name: ActivateBlogPost :exec
UPDATE blog_posts
set is_public = 1
WHERE ID = ?;

